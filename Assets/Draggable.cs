using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ObjectSettings>().OnDroppedSuccessfully.AddListener(() =>
        {
            GameManager.main.AddPoint(GetComponent<ObjectSettings>());
            DragDropManager.main.RemoveObj(GetComponent<ObjectSettings>());
            //GetComponent<ObjectSettings>().enabled = false;
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
