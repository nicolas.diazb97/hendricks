using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    public GameObject rose;
    public GameObject parent;
    private List<Position> positions;
    public GameObject positionHandler;
    public int roseCounter;
    public List<string> collectedIDs = new List<string>();
    public Text pointsCounterText;
    int counter = 0;
    int tempCounter = 0;
    private List<Rigidbody2D> roses = new List<Rigidbody2D>();

    public bool timerIsOn = true;

    // Start is called before the first frame update
    void Start()
    {
        positions = positionHandler.GetComponentsInChildren<Position>().ToList();
        InitGame();

    }
    // Update is called once per frame
    void Update()
    {

    }
    public void AddPoint(ObjectSettings _obj)
    {
        Debug.LogError("esto pasa");
        bool alreadyOnQueue = false;
        foreach (var item in collectedIDs)
        {
            if (item.Equals(_obj.Id))
            {
                alreadyOnQueue = true;
            }
        }
        if (!alreadyOnQueue)
        {
            collectedIDs.Add(_obj.Id);
            roseCounter++;
            pointsCounterText.text = roseCounter.ToString();
        }
    }
    public void InitGame()
    {
        InstantiateRoses();
        StartCoroutine(Instantiate());
    }
    public IEnumerator Instantiate()
    {
        if (timerIsOn)
        {
            roses[counter].simulated = true;
            counter++;
            yield return new WaitForSecondsRealtime(0.3f);
            StartCoroutine(Instantiate());
        }
    }
    public void Stop()
    {
        timerIsOn = false;
    }
    public void InstantiateRoses()
    {
        for (int i = 0; i < 400; i++)
        {
            GameObject tempRose = Instantiate(rose, positions[Random.Range(0, positions.Count)].transform.position, transform.rotation, parent.transform);
            roses.Add(tempRose.GetComponent<Rigidbody2D>());
            tempRose.GetComponent<ObjectSettings>().Id = "clone:" + tempCounter;
            tempCounter++;
            DragDropManager.main.AllObjects.Add(tempRose.GetComponent<ObjectSettings>());

        }
        DragDropManager.main.Init();
    }
}
