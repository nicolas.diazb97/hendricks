using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class DataManager : MonoBehaviour
{
    public TMP_InputField email;
    // Start is called before the first frame update

    public void SaveData()
    {
        string savedEmails = PlayerPrefs.GetString("emails", "");
        PlayerPrefs.SetString("emails", savedEmails + email.text + " \n");
        string DataToSend = PlayerPrefs.GetString("emails", "");
        Debug.Log(DataToSend);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/Data.txt", DataToSend);
        SceneManager.LoadScene("Demo");
    }
}
